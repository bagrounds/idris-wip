module Dtipl1

import Data.Vect
%default total

-- idris class by edwin

StringOrNat : (isStr : Bool) -> Type
StringOrNat False = Nat
StringOrNat True = String

lengthOrDouble : (isStr : Bool) -> StringOrNat isStr -> Nat
lengthOrDouble False x = x + x
lengthOrDouble True x = length x

data Format = Number Format
            | Str Format
            | Lit String Format
            | End

%name Format fmt

printfTy : Format -> Type
printfTy (Number fmt) = ?printfTy_rhs_1
printfTy (Str fmt) = ?printfTy_rhs_2
printfTy (Lit x fmt) = ?printfTy_rhs_3
printfTy End = ?printfTy_rhs_4

data List' a = Nil | (::) a (List a)

append' : List a -> List a -> List a
append' [] ys = ys
append' (x :: xs) ys = x :: append' xs ys

append'' : Vect n a -> Vect m a -> Vect (n + m) a
append'' [] ys = ys
append'' (x :: xs) ys = x :: append'' xs ys

map' : (a -> b) -> List a -> List b
map' f xs = ?map'_rhs

zip' : Vect n a -> Vect n b -> Vect n (a, b)
zip' [] [] = []
zip' (x :: xs) (y :: ys) = (x, y) :: zip' xs ys

zipWith' : (a -> b -> c) -> Vect n a -> Vect n b -> Vect n c
zipWith' f [] [] = []
zipWith' f (x :: xs) (y :: ys) = f x y :: zipWith' f xs ys

transpose_mat : Vect n (Vect m elem) -> Vect m (Vect n elem)
transpose_mat [] = empties where
  empties : Vect m (Vect 0 elem)
  empties {m = Z} = []
  empties {m = (S k)} = [] :: empties
transpose_mat (x :: xs) =
  let xs_trans = transpose_mat xs in
      helper x xs_trans where
        helper : (x : Vect m elem) ->
                 (xs_trans : Vect m (Vect len elem)) ->
                 Vect m (Vect (S len) elem)
        helper [] [] = []
        helper (x :: xs) (y :: ys) = (x :: y) :: helper xs ys


