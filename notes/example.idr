
add : (a: Nat) -> (b : Nat) -> Nat
add Z k = k
add j Z = j
add (S k) j = S (add k j)

