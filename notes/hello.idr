module Main

import public Data.Vect
import public Data.Matrix

%default total

main : IO ()
main = putStrLn "Hello world"

id : x -> x
id x = x

mul : Num a => Vect n a -> Vect n a -> Vect n a
mul = zipWith (*)

add : Num a => Vect n a -> Vect n a -> Vect n a
add = zipWith (+)

dot : Num a => Vect n a -> Vect n a -> a
dot x y = foldl (+) 0 (mul x y)

take' : Vect (m + n) a -> Vect m a
take' {m=Z} xs = []
take' {m=S m'} (x :: xs) = x :: take' xs

drop' : Vect (m + n) a -> Vect n a
drop' {m=Z} xs = xs
drop' {m=S m'} (x::xs) = drop' xs

rectangularize : Vect (m * n) a -> Vect m (Vect n a)
rectangularize {m=Z} [] = []
rectangularize {m=S m'} xs = take' xs :: rectangularize (drop' xs)

-- fromTo : (a : Nat) -> (b: Nat) -> Vect ((b - a) + 1) Nat
-- fromTo a b = fromList [a..(a + b)]

downFrom : (a : Nat) -> Vect a Nat
downFrom Z = []
downFrom (S k) = k :: (downFrom k)

upTo : (a : Nat) -> Vect a Nat
upTo n = reverse (downFrom n)

-- fromTo : (a : Nat) -> (b: Nat) -> Vect (b - a) Nat
-- fromTo a b = ?fromTo_rhs

-- fromTo Z Z = []
-- fromTo Z (S Z) = [Z]
-- fromTo Z (S k) = [k] ++ (fromTo Z k)
--   where
--     P : (n : Nat) -> minus n 0 = n
--     P = minusZeroRight
-- fromTo k k = []
-- fromTo Z (S Z) = [1]
-- fromTo Z (S (S Z)) = (fromTo Z (S Z)) ++ [2]

-- Note that mSmallest is accepted as total with just this one case!
mSmallest : (n : Nat) -> (m : Nat) -> {auto p : n `GT` m} -> Vect m (Fin n)
mSmallest (S k) m {p = LTESucc p} = replicate m FZ

mkRect : (a : Nat) -> (b : Nat) -> Matrix a b Nat
mkRect a b = rectangularize $ upTo (a * b)

foldr' : (elt -> acc -> acc) -> acc -> Vect n elt -> acc
foldr' f z [] = z
foldr' f z (y :: xs) = f y (foldr' f z xs)

product' : Num a => Vect n a -> a
product' = foldr' (*) 1 

-- Then we can do the same for arbitrary-dimensional arrays:

cubify : {ds : Vect n Nat} -> Vect (product' ds) a -> foldr' Vect a ds
cubify {ds=[]} [x] = x
cubify {ds=d::ds'} xs = map cubify (rectangularize xs)

-- The inverse operation of course:

nconcat : {ds : Vect n Nat} -> foldr' Vect a ds -> Vect (product' ds) a
nconcat {ds=[]} x = [x]
nconcat {ds=d::ds'} xs = concat (map nconcat xs) 

f : (n: Nat) -> Vect (n + n) a -> Matrix 2 n a
f n v = [(take n v)] ++ [(drop n v)]

--f2 : (n: Nat) -> Vect (n * m) a -> Matrix n m a
--f2 n v = f2'' (Vect.replicate m v)
--  where
--    f2' : Nat -> Vect n a
--    f2' i = take n (drop (i * n) v)
--    f2'' : Vect m Nat -> Matrix n m a
--    f2'' v' = map f2' v'

sum' : Num x => List x -> x
sum' xs = foldl (+) 0 xs

-- f2 : (n: Nat) -> Vect (n + n) a -> Matrix 2 n a
-- f2 n v = [take n v, drop n v]

-- vec2mat : (r : Nat) -> Vect ?rIn a -> Matrix ?rOut ?cOut a
-- vec2mat r v = fst (f ([], v)) where
--   f : (Matrix ?a ?b a, Vect ?c a) -> (Matrix ?d ?e a, Vect ?f a)
--   f (mat, vec) = case vec of
--     Vect.Nil => (mat, [])
--     _  => f (mat ++ [(take r vec)], Data.Vect.drop r vec)

-- idris class by edwin
-- module Dtipl1

public export
StringOrNat : (isStr : Bool) -> Type
-- Try case splitting this with the editor commands
-- StringOrNat isStr = ?StringOrNat_rhs
StringOrNat False = Nat
StringOrNat True = String

lengthOrDouble : (isStr : Bool) -> StringOrNat isStr -> Nat
lengthOrDouble isStr x = ?lengthOrDouble_rhs
