module Dtipl2

import Data.Vect

%default total

-- idris class by edwin

data EqNat : Nat -> Nat -> Type where
  SameNat : (num : Nat) -> EqNat num num

smallProofEq : EqNat (2 + 2) 4
smallProofEq = SameNat 4

-- smallProofEq' : EqNat (2 + 2) 5
-- smallProofEq' = smallProofEq'

successorEq : EqNat x y -> EqNat (S x) (S y)
successorEq (SameNat x) = SameNat (S x)

smallProof : 2 + 2 = 4
smallProof = Refl

notTrue : 2 + 2 = 5 -> Void
notTrue Refl impossible

tryZip : Vect n a -> Vect m b -> Maybe (Vect n (a, b))
tryZip {n = Z} {m = Z} [] [] = Just []
tryZip {n = Z} {m = (S k)} [] (x :: xs) = Nothing
tryZip {n = (S k)} {m = Z} (x :: xs) [] = Nothing
tryZip {n = (S k)} {m = (S j)} (x :: xs) (y :: ys) = f (x, y) (tryZip xs ys)
  where
    f : (a, b) -> Maybe (Vect k (a, b)) -> Maybe (Vect (S k) (a, b))
    f (a, b) Nothing = Nothing
    f (a, b) (Just x) = Just $ (a, b) :: x

checkEqNat : (n : Nat) -> (m : Nat) -> Maybe (n = m)
checkEqNat Z Z = Just Refl
checkEqNat Z (S k) = Nothing
checkEqNat (S k) Z = Nothing
checkEqNat (S k) (S j) = case checkEqNat k j of
                              Nothing => Nothing
                              (Just Refl) => Just Refl

tryZip' : Vect n a -> Vect m b -> Maybe (Vect n (a, b))
tryZip' {n} {m} xs ys =
  case checkEqNat n m of
       Nothing => Nothing
       (Just Refl) => Just $ zip xs ys

zeroNotSucc : (0 = S k) -> Void
zeroNotSucc Refl impossible

succNotZero : (S k = 0) -> Void
succNotZero Refl impossible

successorFail : (contra : (k = j) -> Void) -> (S k = S j) -> Void
successorFail contra Refl = contra Refl

checkEqNat' : (n : Nat) -> (m : Nat) -> Dec (n = m)
checkEqNat' Z Z = Yes Refl
checkEqNat' Z (S k) = No zeroNotSucc
checkEqNat' (S k) Z = No succNotZero
checkEqNat' (S k) (S j)
  = case checkEqNat' k j of
         (Yes Refl) => Yes Refl
         (No contra) => No (successorFail contra)

tryZip'' : Vect n a -> Vect m b -> Maybe (Vect n (a, b))
tryZip'' {n} {m} xs ys = case checkEqNat' n m of
                              (Yes Refl) => Just $ zip xs ys
                              (No contra) => Nothing

data Elem' : a -> List a -> Type where
  Here' : Elem' x (x :: xs)
  There' : Elem' x ys -> Elem' x (y :: ys)

Beatles : List String
Beatles = ["John", "Paul", "George", "Ringo"]

georgeInBeatles : Elem' "George" Beatles
georgeInBeatles = There' (There' Here')

peteNotInBeatles : Not (Elem' "Pete" Beatles)
peteNotInBeatles (There' (There' (There' (There' Here')))) impossible
peteNotInBeatles (There' (There' (There' (There' (There' _))))) impossible

checkEq : DecEq x => DecEq y => x -> y -> Dec (x = y)
checkEq x y = case x = y of
                   case_val => ?checkEq_rhs_1


f : Elem x (y :: xs)
f = ?f_rhs

isElem : DecEq a => (x : a) -> (xs : Vect k a) -> Maybe (Elem x xs)
isElem x [] = Nothing
isElem x (y :: xs) = case (x `checkEq` y) of
                          (Yes Refl) => Just (Elem x (y :: xs))
                          (No contra) => Nothing
                    

