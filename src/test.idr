module Test

import Effects
import Effect.System
import Effect.Random
import linear

%access public export

assertEq : Show a => Eq a => (given : a) -> (expected : a) -> IO ()
assertEq g e =
  let msg =
    if g == e
       then ("ok - " ++ (show g) ++ " == " ++ (show e))
       else ("not ok - " ++ (show g) ++ " != " ++ (show e))
       in putStrLn msg

Rand : Type -> Type
Rand a = Eff a [RND,SYSTEM]

randInit : Rand ()
randInit = do 
  t <- time
  srand t

randInt : Rand Integer
randInt = do
  rndInt 0 100

randPair : Rand a -> Rand (a, a)
randPair gen = do
  i1 <- gen
  i2 <- gen
  pure (i1, i2)

randVect : (n : Nat) -> Rand a -> Rand (Vect n a)
randVect Z g = pure []
randVect (S k) g = do
  i1 <- g
  v <- randVect k g
  pure (i1 :: v)

randMat : (m : Nat) -> (n : Nat) -> Rand a -> Rand (Matrix m n a)
randMat a b = randVect a . randVect b

intToDouble : Integer -> Double
intToDouble = Cast.cast {to=Double}

test : IO ()
test = do
  m <- run $ do
    randInit
    randMat 3 3 randInt
  assertEq (id 3 `mul` m) m
  assertEq (m `mul` id 3) m
  assertEq (map ((mapMat intToDouble m) `mul`) (inverse (mapMat intToDouble m))) (Just (id 3))

