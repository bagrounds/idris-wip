module Linear

import public Data.Vect
import public Data.Matrix

%default total
%access public export

inner : Num a => Vect n a -> Vect n a -> a
inner xs ys = foldl (+) 0 $ zipWith (*) xs ys

scale : Num a => a -> Vect n a -> Vect n a
scale = map . (*)

outer : Num a => Vect n a -> Vect m a -> Matrix n m a
outer [] _ = []
outer (x :: xs) ys = scale x ys :: outer xs ys

mapCols : Num a => (Vect m a -> b) -> Matrix m n a -> Vect n b
mapCols f = map f . transpose

mulvm : Num a => Vect m a -> Matrix m n a -> Vect n a
mulvm = mapCols . inner

mulmv : Num a => Matrix m n a -> Vect n a -> Vect m a
mulmv mat = flip map mat . inner

index : (m : Nat) -> (n : Nat) -> Matrix m n (Fin m, Fin n)
index m n = map (\i => map (\j => (i, j)) (range {len=n})) (range {len=m})

indexNats : (m : Nat) -> (n : Nat) -> Matrix m n (Nat, Nat)
indexNats m n = map (\i => map (\j => (i, j)) (map finToNat (range {len=n}))) (map finToNat (range {len=m}))

mapMat : (a -> b) -> Matrix m n a -> Matrix m n b
mapMat = map . map

scaleMat : Num a => a -> Matrix m n a -> Matrix m n a
scaleMat = mapMat . (*)

--indexFins : Fin (S m) ->
--            Fin (S n) ->
--            Matrix (S m) (S n) (Fin (S m), Fin (S n))
--indexFins {m} {n} (FS fm) (FS fn) = mapMat (\(a, b) => ((n2f a fm), (n2f b fn))) (indexNats (S m) (S n))
--  where
--    n2f : Nat -> Fin top -> Fin (S top)
--    n2f Z _ = FZ
--    n2f (S k) FZ = n2f k FZ
--    n2f (S k) (FS t) = FS (n2f k t)
--    n2f {top} x t = weakenN 0 {m=top} t

mul : Num a => Matrix r i a -> Matrix i c a -> Matrix r c a
mul {r} {c} x y = mapMat dot (index r c) where
  dot : (Fin r, Fin c) -> a
  dot (i, j) = inner (getRow i x) (getCol j y)

id : Num a => (d: Nat) -> Matrix d d a
id d = mapMat zeroOrOne (index d d) where
  zeroOrOne : (Fin n, Fin m) -> a
  zeroOrOne (a, b) = if ((finToNat a) == (finToNat b)) then 1 else 0

showVect : Show a => Vect n a -> String
showVect x = "[" ++ (foldl (++) "" (stringify x)) ++ "]"
  where
    stringify : Show a => Vect n a -> List String
    stringify x = intersperse " " (map show (toList x))

showMat : Show a => Matrix m n a -> String
showMat x = (foldl (++) "" (stringify x))
  where
    stringify : Show a => Matrix m n a -> List String
    stringify x = intersperse "\n" (map showVect $ toList x)

ij : Fin m -> Fin n -> Matrix m n a -> a
ij x y z = Data.Vect.index y $ Data.Vect.index x z

Square : Nat -> Type -> Type
Square x = Matrix x x

stack : Matrix m n a -> Vect (m * n) a
stack [] = []
stack (x :: xs) = x ++ stack xs

det : Neg a => Num a => Square d a -> a
det [] = 1
det [[x]] =  x
det [[a, b], [c, d]] = (a * d) - (b * c)
det {d=(S (S k))} x = sum $ (map (\i => (Prelude.pow (-1) $ finToNat i) * (ij FZ i x) * (det $ subMatrix FZ i x)) (range {len=(S (S k))}))

minor : Neg a => Num a => Fin (S (S d)) -> Fin (S (S d)) -> Square (S (S d)) a -> a
minor x y z = det $ subMatrix x y z

zipMat : Matrix m n a -> Matrix m n b -> Matrix m n (a, b)
zipMat a b = zipWith zip a b

zipMatWith : (a -> b -> c) -> Matrix m n a -> Matrix m n b -> Matrix m n c
zipMatWith = zipWith . zipWith

cartesian : Vect m a -> Vect n b -> Matrix m n (a, b)
cartesian v1 v2 = map (\v1e => map (\v2e => (v1e, v2e)) v2) v1

grid : (m: Nat) -> (n: Nat) -> Matrix m n (Fin m, Fin n)
grid a b = cartesian (fins a) (fins b)

iMapVect : (a -> Nat -> b) -> Vect n a -> Vect n b
iMapVect f [] = []
iMapVect f (x :: xs) = f x 0 :: iMapVect g xs
  where
    g : a -> Nat -> b
    g a m = f a (m + 1)
 
subMatrices : Matrix (S m) (S n) a ->
              Matrix (S m) (S n) (Matrix m n a)
subMatrices {m} {n} input =
  mapMat (\(a, b) => subMatrix a b input) (grid (S m) (S n))

minors : Neg a => Num a => Square (S d) a -> Square (S d) a
minors input = mapMat det (subMatrices input)

cofactorSigns : Neg a => Num a => Matrix m n a -> Matrix m n a
cofactorSigns {m} {n} _ = mapMat (\(a, b) => Prelude.pow (-1) ((fromInteger (finToInteger a)) + (fromInteger (finToInteger b)))) (grid m n)

cofactors : Neg a => Num a => Square (S d) a -> Square (S d) a
cofactors input = zipMatWith (*) (minors input) (cofactorSigns input)

inverse : Num a => Neg a => Fractional a => Square (S d) a ->
          Maybe (Square (S d) a)
inverse m = case (det m) of
                 0 => Nothing
                 d => Just ((/ d) `mapMat` (transpose (cofactors m)))


